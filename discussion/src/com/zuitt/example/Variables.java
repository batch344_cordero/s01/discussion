package com.zuitt.example;

public class Variables {
    public static void main (String[] args){
        int age;
        char middleName;

        int x;
        int y = 0;

        x = 1;

        System.out.println("The value of y is " + y + " and the value of x is " + x);

        int wholeNumber = 100;
        System.out.println(wholeNumber);

        long worldPopulation = 47821468712476L;
        System.out.println(worldPopulation);

        float piFloat = 3.14159265359f;
        System.out.println("The Value of piFloat is " + piFloat);

        double piDouble = 3.14159265359f;
        System.out.println("The Value of piFloat is " + piDouble);

        char letter = 'a';
        System.out.println(letter);

        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        String username = "JSmith";
        System.out.println(username);

        int stringLength = username.length();
        System.out.println(stringLength);

        
    }
}
